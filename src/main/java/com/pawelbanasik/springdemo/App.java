package com.pawelbanasik.springdemo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class App {
	public static void main(String[] args) {

		ApplicationContext context = new AnnotationConfigApplicationContext(ConfigProperty.class);
		
		Organization organization = (Organization) context.getBean("myorg");
		
		organization.corporateSlogan();

		System.out.println(organization);
		
		((AbstractApplicationContext) context).close();
		
	}
}
