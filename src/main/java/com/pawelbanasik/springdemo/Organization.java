package com.pawelbanasik.springdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("myorg")
public class Organization {

	@Value("${companyName}")
	private String companyName;

	@Value("${yearOfIncorporation}")
	private int yearOfIncorporation;

	public Organization() {
		System.out.println("Empty constructor called. Reflexion needs empty constructor.");
	}

	
	public Organization(String companyName, int yearOfIncorporation) {
		this.companyName = companyName;
		this.yearOfIncorporation = yearOfIncorporation;
	}

	public void corporateSlogan() {
		String slogan = "We build the ultimate driving machines";
		System.out.println(slogan);
	}

	// public void setCompanyName(String companyName) {
	// this.companyName = companyName;
	// }
	//
	// public void setYearOfIncorporation(int yearOfIncorporation) {
	// this.yearOfIncorporation = yearOfIncorporation;
	// }

	@Override
	public String toString() {
		return "Organization [companyName=" + companyName + ", yearOfIncorporation=" + yearOfIncorporation + "]";
	}

}
